<?php

namespace app\controllers;

use Yii;
use app\models\Avance;

use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use mPDF;


class ReportController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */


    public function actionAvance()
    {

        if (Yii::$app->user->isGuest) {

            throw new NotFoundHttpException('No a Iniciado Sesión...');
        }else {

            $model = new Avance();

            return $this->render('form/avance_form', ['model' => $model,]);
        }

    }

    public function actionPoa()
    {

        if (Yii::$app->user->isGuest) {

            throw new NotFoundHttpException('No a Iniciado Sesión...');
        }else {

            $model = new Avance();

            return $this->render('form/poa_form', ['model' => $model,]);
        }

    }


    public function actionMc()
    {

        if (Yii::$app->user->isGuest) {

            throw new NotFoundHttpException('No a Iniciado Sesión...');
        }else {

            $model = new Avance();
            $this->layout = 'pdf/pdf';
            return $this->render('mc', ['model' => $model,]);
        }

    }

    public function actionMc2()
    {

        if (Yii::$app->user->isGuest) {

            throw new NotFoundHttpException('No a Iniciado Sesión...');
        }else {

            $model = new Avance();
            $this->layout = 'pdf/pdf';
            return $this->render('mc2', ['model' => $model,]);
        }

    }

    public function actionInfgest()
    {

        if (Yii::$app->user->isGuest) {

            throw new NotFoundHttpException('No a Iniciado Sesión...');
        }else {

            $model = new Avance();
            $this->layout = 'pdf/pdf';
            return $this->render('infgest', ['model' => $model,]);
        }

    }

    public function actionInformef($id)
    {

        if (Yii::$app->user->isGuest) {

            throw new NotFoundHttpException('No a Iniciado Sesión...');
        }else {

          //  $model = new Avance();
            $this->layout = 'pdf/pdf';
            return $this->render('informef', ['id' => $id,]);
        }

    }

    public function actionAvancepdf($poa,$id_dtpoa,$trimestre)
    {

        if (Yii::$app->user->isGuest) {

           // return SiteControlleractionLogin();
            throw new NotFoundHttpException('No a Iniciado Sesión...');
        }else {

            $mpdf=new mPDF('c','A4-L');
            $mpdf->SetHeader('{DATE j-m-Y}');
            $mpdf->pagenumPrefix='Pág. ';
            $mpdf->pagenumSuffix=' de ';
            $mpdf->SetFooter('{PAGENO}{nbpg}');
            $this->layout = 'pdf/pdf';

            $mpdf->WriteHTML($this->render('avancepdf', [
                'poa' => $poa,
                'id_dtpoa' => $id_dtpoa,
                'trimestre' => $trimestre,
            ]));
            $mpdf->Output();
        }

    }




    public function actionPoapdf($poa)
    {

        if (Yii::$app->user->isGuest) {

           // return SiteControlleractionLogin();
            throw new NotFoundHttpException('No a Iniciado Sesión...');
        }else {

            $mpdf=new mPDF('c','A4-L');
            $mpdf->SetHeader('{DATE j-m-Y}');
            $mpdf->pagenumPrefix='Pág. ';
            $mpdf->pagenumSuffix=' de ';
            $mpdf->SetFooter('{PAGENO}{nbpg}');
            $this->layout = 'pdf/pdf';

            $mpdf->WriteHTML($this->render('poapdf', [
                'poa' => $poa,

            ]));
            $mpdf->Output();
        }

    }

}
