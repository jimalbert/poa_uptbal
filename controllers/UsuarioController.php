<?php

namespace app\controllers;

use Yii;
use app\models\Usuario;
use app\models\UsuarioSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\User;
/**
 * UsuarioController implements the CRUD actions for Usuario model.
 */
class UsuarioController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
        'access' => [
                'class' => AccessControl::className(),
                'rules' => [

                    [
                        'actions' => ['index', 'view','create', 'update','delete'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            //Llamada al método que comprueba si es un usuario del rol permitido
                            return User::isUserAdmin(Yii::$app->user->identity->id);
                        },
                    ],
                    [
                        'actions' => ['index', 'view','create', ],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            //Llamada al método que comprueba si es un usuario del rol permitido
                            return User::isUserTecnico(Yii::$app->user->identity->id);
                        },
                    ],
                    [
                        'actions' => ['index', 'view','create', 'update',],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            //Llamada al método que comprueba si es un usuario del rol permitido
                            return User::isUserSup(Yii::$app->user->identity->id);
                        },
                    ],
                ],
            ],

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Usuario models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {

            throw new NotFoundHttpException('No a Iniciado Sesión...');
        }else {
            $searchModel = new UsuarioSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Displays a single Usuario model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }



    /**
     * Creates a new Usuario model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->user->isGuest) {

            throw new NotFoundHttpException('No a Iniciado Sesión...');
        }else {
            $model = new Usuario();



            if ($model->load(Yii::$app->request->post()) ) {
$model->cedula=$model->ci_tecnico;
                if($model->password!=''){
                $model->password = crypt($model->password, Yii::$app->params["salt"]);

                }

                if ( $model->save()) {

                   return $this->redirect(['index']);
                }
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Updates an existing Usuario model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (Yii::$app->user->isGuest) {

            throw new NotFoundHttpException('No a Iniciado Sesión...');
        }else {

            $model = $this->findModel($id);

            if ($model->load(Yii::$app->request->post()) ) {

$model->cedula=$model->ci_tecnico;


                if($model->password!=''){
                $model->password = crypt($model->password, Yii::$app->params["salt"]);

                }

                if ( $model->save()) {

                   return $this->redirect(['index']);
                }else{
$model->save();
                        $errores = "";
                    
                    foreach ( $model->getErrors() as $key => $value ) {
                        foreach ( $value as $row => $field ) {
                            $errores .= $field . "<br>";
                        }
                    }
                    
                    echo $errores;
                    }
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /*public function actionActivador($id)
    {

echo "string".$id;

$model = $this->findModel($id);
      

            if ($model->load(Yii::$app->request->post()) ) {

                if($model->password!=''){
                  $model->password = crypt($model->password, Yii::$app->params["salt"]);

                }
echo 'ddddd'.$model->nombre;
                $model->rol='SUPERVISOR';
                $model->estatus='ACTIVO';

              /*  if ( $model->save()) {

                   return $this->redirect(['/site/index']);
                }*

            } else {
                return $this->render('activa', [
                    'model' => $model,
                ]);
            }

    }*/

    /**
     * Deletes an existing Usuario model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (Yii::$app->user->isGuest) {

            throw new NotFoundHttpException('No a Iniciado Sesión...');
        }else {
            $this->findModel($id)->delete();

            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the Usuario model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Usuario the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Usuario::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
