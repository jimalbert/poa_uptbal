<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Usuario;
use app\models\Areaaccionunidadesponsable;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {
            return $this->actionLogin();
        }else {

          if (Yii::$app->user->identity->rol=="SUPERVISOR"){
              $Areaaccionunidadesponsable=Areaaccionunidadesponsable::find()
              ->where(['id_usuario' => Yii::$app->user->identity->id])->count();

              if ($Areaaccionunidadesponsable<=0) {
                echo $Areaaccionunidadesponsable;
                $this->layout = 'pdf/pdf';
                return $this->redirect('index.php?r=unidadresponsable/regdependencia');
              }else{

                return $this->render('supervisor');
              }
          }else{
              return $this->render('index');
          }
          //  return $this->render('index');
        }
       // return $this->render('index');
    }

    public function actionReport($idcalle)
    {
        //echo $idcalle;
        //Yii::$app->response->format = 'pdf';

        Yii::$app->response->format = 'pdf';

        //Can you it if needed to rotate the page

        $this->layout = 'pdf/pdf';

        return $this->render('planilla', [
            'idcalle' => $idcalle,
        ]);
    }



    public function actionResumencomunidad($idcomunidad,$comunidad)
    {
        //echo $idcalle;
        return $this->render('resumencomunidad', [
            'idcomunidad' => $idcomunidad,
            'comunidad'=> $comunidad,
        ]);
    }
    public function actionResumencomunidadpdf($idcomunidad,$comunidad)
    {
        //echo $idcalle;
        Yii::$app->response->format = 'pdf';

        //Can you it if needed to rotate the page

        $this->layout = 'pdf/pdf';
        return $this->render('resumencomunidadpdf', [
            'idcomunidad' => $idcomunidad,
            'comunidad'=> $comunidad,
        ]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {

            return $this->goHome();

        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

/*    public function actionActivar()
    {
      $model = new Usuario();
      if ($model->load(Yii::$app->request->post()) ) {
          $model2 =  Usuario::find()->where(['ci_tecnico'=>$model->cedula])->one();

      if ($model2) {
        echo $model2->nombre;

        //return Yii::$app->response->redirect(['/usuario/activa', 'id' => $model2->id])->send();

        // return $this->redirect(\Yii::$app->urlManager->createUrl("usuario/activador"));

        # code...
        return $this->render('/usuario/activador', [
            'id' => $model2->id,
        ]);
      }


      } else {
          return $this->render('_form_cuenta', [
              'model' => $model,
          ]);
      }
    }*/

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
