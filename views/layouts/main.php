<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);


?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>

<?php $this->beginBody() ?>

<div class="wrap">

    <?php
    NavBar::begin([
        'brandLabel' => 'PLAN INTEGRAL DE DESARROLLO INSTITUCIONAL',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);

if (Yii::$app->user->isGuest){
        echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [

                ['label' => 'Iniciar Sesión', 'url' => ['/site/login']]
        ],
    ]);

} else{

        if (Yii::$app->user->identity->rol=="ADMINISTRADOR"){

            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => [

                    ['label' => 'Plan Integral de Desarrollo', 'url' => ['#'],
                        'items' => [
                            ['label' => 'Registrar POA ', 'url' => ['/poa/index']],
                            ['label' => 'Registrar Acciones ', 'url' => ['/dtpoa/index']],
                            ['label' => 'Registrar Avances ', 'url' => ['/avance/index']],

                        ],
                    ],

                    ['label' => 'Definiciones', 'url' => ['#'],

                         'items' => [
                            ['label' => 'Area de Acción', 'url' => ['/areaccion/index']],
                            ['label' => 'Unidad de Medida', 'url' => ['/unidadmedida/index']],
                            ['label' => 'Unidad Responsable', 'url' => ['/unidadresponsable/index']],
                            ['label' => 'Usuarios', 'url' => ['/usuario/index']],

                            ],
                     ],
                     ['label' => 'Reportes', 'url' => ['#'],

                         'items' => [
                            ['label' => 'Poa', 'url' => ['/report/poa']],
                            ['label' => 'Avances', 'url' => ['/report/avance']],
                            ['label' => 'Informe de Gestión', 'url' => ['/report/infgest']],
                            /*['label' => 'Memoria y Cuenta Formato 1', 'url' => ['/report/mc']],
                            ['label' => 'Memoria y Cuenta Formato 2', 'url' => ['/report/mc2']],*/

                            /*['label' => 'Unidad de Medida', 'url' => ['/unidadmedida/index']],
                            ['label' => 'Unidad Responsable', 'url' => ['/unidadresponsable/index']],
                            ['label' => 'Usuarios', 'url' => ['/usuario/index']],
                            ['label' => 'Cuentas Bancarias', 'url' => ['/cuenta/index']],
                            ['label' => 'Configuración', 'url' => ['/configuracion/update']],*/

                            ],
                     ],
                     Yii::$app->user->isGuest ? (
                        ['label' => 'Iniciar Sesión', 'url' => ['/site/login']]
                    ) : (
                        '<li>'
                        . Html::beginForm(['/site/logout'], 'post')
                        . Html::submitButton(
                            'Cerrar Sesión (' . Yii::$app->user->identity->username . ')',
                            ['class' => 'btn btn-link logout']
                        )
                        . Html::endForm()
                        . '</li>'
                    )
                ],
            ]);
        }

        if (Yii::$app->user->identity->rol=="SUPERVISOR"){

            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => [

                    ['label' => 'Plan Integral de Desarrollo', 'url' => ['#'],
                        'items' => [
                            ['label' => 'Registrar PIDIPA ', 'url' => ['/poa/registro']],
                            ['label' => 'Consultar PIDIPA ', 'url' => ['/poa/index2']],
                            ['label' => 'Registrar Acciones ', 'url' => ['/dtpoa/registro']],
                            ['label' => 'Registrar Avances ', 'url' => ['/avance/registro']],

                        ],
                    ],

                    ['label' => 'Definiciones', 'url' => ['#'],

                         'items' => [

                            ['label' => 'Unidad Responsable', 'url' => ['/unidadresponsable/update2']],

                            ],
                     ],
                     ['label' => 'Reportes', 'url' => ['#'],

                         'items' => [
                            ['label' => 'Plan Integral de Desarrollo', 'url' => ['/report/poa']],
                            ['label' => 'Avances', 'url' => ['/report/avance']],
                            ['label' => 'Informe de Gestión', 'url' => ['/report/infgest']],
                            /*['label' => 'Memoria y Cuenta Formato 1', 'url' => ['/report/mc']],
                            ['label' => 'Memoria y Cuenta Formato 2', 'url' => ['/report/mc2']],*/

                            /*['label' => 'Unidad de Medida', 'url' => ['/unidadmedida/index']],
                            ['label' => 'Unidad Responsable', 'url' => ['/unidadresponsable/index']],
                            ['label' => 'Usuarios', 'url' => ['/usuario/index']],
                            ['label' => 'Cuentas Bancarias', 'url' => ['/cuenta/index']],
                            ['label' => 'Configuración', 'url' => ['/configuracion/update']],*/

                            ],
                     ],
                     Yii::$app->user->isGuest ? (
                        ['label' => 'Iniciar Sesión', 'url' => ['/site/login']]
                    ) : (
                        '<li>'
                        . Html::beginForm(['/site/logout'], 'post')
                        . Html::submitButton(
                            'Cerrar Sesión (' . Yii::$app->user->identity->username . ')',
                            ['class' => 'btn btn-link logout']
                        )
                        . Html::endForm()
                        . '</li>'
                    )
                ],
            ]);
        }


    }
    NavBar::end();
    ?>

    <div class="container">
      <?php echo Html::img('@web/images/uptbal.gif', ['alt' => 'Universidad Politécnica Territorial de Barlovento “Argelia Laya” ','title' => 'Universidad Politécnica Territorial de Barlovento “Argelia Laya” '])?>


        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; UPTBAL <?= date('Y') ?> - Universidad Politécnica Territorial de Barlovento “Argelia Laya”</p>

        <p class="pull-right"><?= "Desarrollado por: Alberto Jiménez / e-mail:jim.alberto@gmail.com"//Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
