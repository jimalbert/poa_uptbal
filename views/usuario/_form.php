<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model app\models\Usuario */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="usuario-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ci_tecnico')->textInput() ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'apellido')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'telefono')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>

     <?= $form->field($model, 'rol')->widget(Select2::classname(), [
        'data' => ['ADMINISTRADOR'=>'ADMINISTRADOR', 'SUPERVISOR'=>'SUPERVISOR', 'TECNICO'=>'TECNICO',],
        'language' => 'es',
        'options' => ['placeholder' => 'Selecione una opción ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>

    <?= $form->field($model, 'estatus')->widget(Select2::classname(), [
        'data' => ['ACTIVO'=>'ACTIVO', 'INACTIVO'=>'INACTIVO', 'SUSPENDIDO'=>'SUSPENDIDO',],
        'language' => 'es',
        'options' => ['placeholder' => 'Selecione una opción ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Registrar' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
