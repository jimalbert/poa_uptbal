<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Iniciar Sesión';

?>
<div class="site-login " align="center">

        <div class="col-md-12 " align="center">
            <div class="panel panel-default " align="center">
                <div class=" user" align="center">
                    <h3 class="panel-title">
                    <i class="glyphicon glyphicon-user"></i>
                    <?= Html::encode($this->title) ?></h3>
                </div>
                <div class="panel-body " align="center">
                     <p>Debe ingresar su Usuario y Contraseña para Acceder al Sistema...</p>

                     <div align="center" >
                    <?php $form = ActiveForm::begin([
                        'id' => 'login-form',
                        'layout' => 'horizontal',
                        'fieldConfig' => [
                            'template' => "{label}\n<div class=\"col-lg-5\">{input}</div>\n<div class=\"col-lg-9\">{error}</div>",
                            'labelOptions' => ['class' => 'col-lg-4 control-label'],
                        ],
                    ]); ?>

                        <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

                        <?= $form->field($model, 'password')->passwordInput() ?>

                        <?= $form->field($model, 'rememberMe')->checkbox([
                            'template' => "<div class=\"col-lg-offset-4 col-lg-3\">{input} {label}</div>\n<div class=\"col-lg-8\">{error}</div>",
                        ]) ?>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-1 col-lg-11">
                                <?= Html::submitButton('Aceptar', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>


                            </div>


                        </div>

                    <?php ActiveForm::end(); ?>
                </div>

                <!-- <div class="col-lg-offset-1 col-lg-11">
                  </br>
                    <?php //= //Html::a('REGISTRAR CUENTA DE USUARIO', ['site/activar', ], ['class' => 'btn btn-success']) ?>

                </div> -->
            </div>
        </div>

</div>
