<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="site-error">

    <h3 class="modal-header-danger"><?= Html::encode($this->title) ?></h3>

    

    <div class="alert alert-danger ">
       <i class="glyphicon glyphicon-remove-sign"></i> <?= nl2br(Html::encode($message)) ?>
    </div>

    

</div>
