<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Areaccion */
/* @var $form yii\widgets\ActiveForm */
$jsc = <<< JS

$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();

$("#texto").fadeIn(3000);
//$("#texto").fadeOut(3000);
});



JS;

$this->registerJs($jsc, $this::POS_END);

$this->title = 'Activar Cuenta de Usuario';

?>

<div id="texto" class="areaccion-form form">




  <h3 class="modal-header-danger"><?= Html::encode($this->title) ?></h3>

    <?php $form = ActiveForm::begin(); ?>
    <?= Html::activeLabel($model, 'cedula', ['class' => ''])?>
    <span  class="badge2 " data-toggle="tooltip" data-placement="right"
    title="DEBE INGRESAR SU CÉDULA DE IDENTIDAD, SI NO EXISTE SU NÚMERO DE CÉDULA DE IDENTIDAD, POR FAVOR COMUNIQUESE CON LA UNIDAD DE SISTEMAS.">
    ?
    </span>
    <?= $form->field($model, 'cedula')->textInput()->label(false) ?>

    <div class="form-group">

        <?= Html::submitButton($model->isNewRecord ? 'Registrar' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <div id="message">

          
             <?php if (Yii::$app->session->hasFlash('error')): ?>
              <div class="alert alert-danger alert-dismissable">
                  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                      <h4><i class="glyphicon glyphicon-remove-sign"></i>Error! </h4>
                      <?= Yii::$app->session->getFlash('error') ?>
              </div>
            <?php endif; ?>

          </div>


</div>
