<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Poa */

$this->title = 'Registrar Plan Integral de Desarrollo';
$this->params['breadcrumbs'][] = ['label' => 'Poas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="poa-create">

     <h3 class="modal-header-danger"><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
