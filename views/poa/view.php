<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Poa */

$this->title = 'Plan Integral de Desarrollo: '.$model->idpoa;

if (Yii::$app->user->identity->rol=="SUPERVISOR") {
  $this->params['breadcrumbs'][] = ['label' => 'Planes Operativos', 'url' => ['index2']];

}else {
  $this->params['breadcrumbs'][] = ['label' => 'Planes Operativos', 'url' => ['index']];

}
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="poa-view">

    <h3 class="modal-header-danger"><?= Html::encode($this->title) ?></h3>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->idpoa], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->idpoa], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'idpoa',
            [
                'attribute' => 'idUnidadDesc',
                'label'     => 'Dependencia',

                ],
            'ano',
            'lineamiento:ntext',
            'objetivo_esrategico:ntext',

          //  'objetivo_parroquial:ntext',
            'proyecto:ntext',
            'objetivo_proyecto',
            //'idUnidadDesc',

        ],
    ]) ?>

</div>
