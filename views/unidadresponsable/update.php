<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Unidadresponsable */

$this->title = 'Actualizar Unidad responsable: ' . $model->idunidadresponsble;
$this->params['breadcrumbs'][] = ['label' => 'Unidades responsables', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idunidadresponsble, 'url' => ['view', 'id' => $model->idunidadresponsble]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="unidadresponsable-update">

    <h3 class="modal-header-danger"><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
