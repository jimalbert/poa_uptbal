<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

use app\models\Areaccion;
/* @var $this yii\web\View */
/* @var $model app\models\Unidadresponsable */
/* @var $form yii\widgets\ActiveForm */

$jsc = <<< JS

$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});
JS;

$this->registerJs($jsc, $this::POS_END);


?>

<div class="unidadresponsable-form">

    <?php $form = ActiveForm::begin(); ?>


    <?= Html::activeLabel($model2, 'id_areaccion', ['class' => ''])?>
    <span  class="badge2 " data-toggle="tooltip" data-placement="right"
    title="ÁREA EN LA QUE SE DESEMPEÑA LA DEPNDECIA.">
    ?
    </span>
    <?=  $form->field($model2, 'id_areaccion')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Areaccion::find()->orderBy('descripcion ASC')->all(), 'idareaccion','descripcion'),
        'language' => 'es',
        'options' => ['placeholder' => 'Selecione una opción ...',

        ],
        'pluginOptions' => [
            'allowClear' => true,
        ],
    ])->label(false);
    ?>


    <?= Html::activeLabel($model, 'descripcion', ['class' => ''])?>
    <span  class="badge2 " data-toggle="tooltip" data-placement="right"
    title="NOMBRE DE UNIDAD, OFICINA, DIRECCIÓN O DEPARTAMENTO ASIGNADO A LA DEPENDENCIA.">
    ?
    </span>
    <?= $form->field($model, 'descripcion')->textInput(['maxlength' => true])->label(false) ?>


    <?= Html::activeLabel($model, 'definicion', ['class' => ''])?>
    <span  class="badge2 " data-toggle="tooltip" data-placement="right"
    title="CONCEPTUALIZACIÓN QUE CARACTERISE EL FUNCIONAMIENTO DE LA DEPENDENCIA.">
    ?
    </span>
    <?= $form->field($model, 'definicion')->textarea(['rows' => 6])->label(false) ?>


    <?= Html::activeLabel($model, 'vision', ['class' => ''])?>
    <span  class="badge2 " data-toggle="tooltip" data-placement="right"
    title="ES HACIA DÓNDE SE QUIERE PROYECTAR LAS ACCIONES DE LA DEPENDENCIA DESDE LOS INVOLUCRADOS.">
    ?
    </span>
   <?= $form->field($model, 'vision')->textarea(['rows' => 6])->label(false) ?>


   <?= Html::activeLabel($model, 'mision', ['class' => ''])?>
   <span  class="badge2 " data-toggle="tooltip" data-placement="right"
   title="ES EL ENCARGO SOCIAL QUE DEBE CUMPLIR LA DEPENDECIA EN FUNCIÓN A LOS PLANES ESTRATÉGICOS DE LA NACIÓN, EL ESTADO Y EL TERRITORIO.">
   ?
   </span>
   <?= $form->field($model, 'mision')->textarea(['rows' => 6])->label(false) ?>


   <?= Html::activeLabel($model, 'objetivo_general', ['class' => ''])?>
   <span  class="badge2 " data-toggle="tooltip" data-placement="right"
   title="ES LA ACCIÓN MACRO QUE ESTABLECE A AJECUTAR LA DEPENDECIA.">
   ?
   </span>
   <?= $form->field($model, 'objetivo_general')->textarea(['rows' => 6])->label(false) ?>


   <?= Html::activeLabel($model, 'objetivos_especificos', ['class' => ''])?>
   <span  class="badge2 " data-toggle="tooltip" data-placement="right"
   title="ACCIONES ESPECÍFICAS QUE CONLLEVAN AL LOGRO DEL OBJETIVO GENERAL DE LA DEPENDENCIA.">
   ?
   </span>
   <?= $form->field($model, 'objetivos_especificos')->textarea(['rows' => 6])->label(false) ?>


   <?= Html::activeLabel($model, 'funciones', ['class' => ''])?>
   <span  class="badge2 " data-toggle="tooltip" data-placement="right"
   title="SON LOS MÉTODOS, CAMINOS CON LOS CUALES SE DEBEN INCURSIONAR PARA EL LOGRO DE LOS OBJETIVOS.">
   ?
   </span>
   <?= $form->field($model, 'funciones')->textarea(['rows' => 6])->label(false) ?>


   <?= Html::activeLabel($model, 'lineas_estrategicas', ['class' => ''])?>
   <span  class="badge2 " data-toggle="tooltip" data-placement="right"
   title="DIRECTRICES SECUENCIALES QUE SE PLANTEAN PARA EL RECORRIDO METODOLÓGICO HACIA EL LOGRO DE LOS OBJETIVOS.">
   ?
   </span>
   <?= $form->field($model, 'lineas_estrategicas')->textarea(['rows' => 6])->label(false) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Registrar' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
