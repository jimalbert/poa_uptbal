<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

use app\models\Poa;
use app\models\Dtpoa;
use app\models\Avance;



$Poa=Poa::find()->where(['idpoa' => $poa])->one();



$this->title = 'SISTEMA DE PLANIFICACION - AVANCE DE PLAN OPERATIVO';
//echo $municipio;


echo  Html::img('@web/images/uptbal.gif', ['alt' => 'Universidad Politécnica Territorial de Barlovento “Argelia Laya” ','title' => 'Universidad Politécnica Territorial de Barlovento “Argelia Laya” ']);


?>

<div class="listado-parroquia">

		<div >
              <h3 class="modal-header-danger">
                     AVANCE DEL PLAN INTEGRAL DE DESARROLLO INSTITUCIONAL POLÍTICO ACADÉMICO - 
              </br>
                    <?= strtoupper($Poa->idUnidadDesc) ?>

              </h3>
		</div>



<table class="table table-striped table-bordered">

    <tr>
      <td align="center"><strong><?= Html::activeLabel($Poa, 'ano'); ?></strong></td>
      <td> <?= $Poa->ano ?> </td>
    </tr>

    <tr>
      <td align="center"><strong><?= Html::activeLabel($Poa, 'lineamiento'); ?></strong></td>
      <td> <?= nl2br(strtoupper($Poa->lineamiento)) ?> </td>
    </tr>

    <tr>
      <td align="center"><strong><?= Html::activeLabel($Poa, 'objetivo_esrategico'); ?></strong></td>
      <td> <?= nl2br(strtoupper($Poa->objetivo_esrategico)) ?> </td>
    </tr>



    <tr>
      <td align="center"><strong><?= Html::activeLabel($Poa, 'proyecto'); ?></strong></td>
      <td> <?= nl2br(strtoupper($Poa->proyecto)) ?> </td>
    </tr>
    <tr>
      <td align="center"><strong><?= Html::activeLabel($Poa, 'objetivo_proyecto'); ?></strong></td>
      <td> <?= nl2br(strtoupper($Poa->objetivo_proyecto)) ?> </td>
    </tr>


</table>


<table class="table-striped table-bordered">

    <tr class="bg bg-danger">
      <td align="center"  rowspan="2"><strong>N°</strong></td>
      <td align="center"  rowspan="2"><strong>Acciones</strong></td>
      <td align="center"  rowspan="2"><strong>Unidad de Medida </strong></td>
      <td align="center"  rowspan="2"><strong>Meta Anual</strong></td>

      <td align="center"  colspan="4"><strong>Trimestre de realización</strong></td>

      <td align="center"  rowspan="2"><strong>Efectividad</strong></td>


    </tr >
    <tr class="bg bg-success">
    <td align="center"><strong>I</strong></td>
    <td align="center"><strong>II</strong></td>
    <td align="center"><strong>III</strong></td>
    <td align="center"><strong>IV</strong></td>
  </tr>

    <?php $Dtpoa=Dtpoa::find()->where(['id_poa' => $poa])->all();

      $Avance=0;
      $efectividad=0;
      $meta_anual=0;
      foreach($Dtpoa as $key => $value) {

        $meta_anual=$value->meta_anual;
        echo "<tr>";
            echo "<td><strong>". strtoupper($key) ."</strong></td>";
            echo "<td>". strtoupper($value->actividad) ."</td>";
            echo "<td align=center>". strtoupper($value->idUnidadMedida->descripcion) ."</td>";
            echo "<td align=center>". number_format($meta_anual, 0, ",", ".") ."</td>";

            $Avancet1=Avance::find()->where(['between','mes', "1","3"])->andFilterWhere(['id_dtpoa' => $value->iddtpoa])->sum('valor');

            echo "<td align=center>". number_format($Avancet1, 0, ",", ".") ."</td>";

            $Avancet2=Avance::find()->where(['between','mes', "4","6"])->andFilterWhere(['id_dtpoa' => $value->iddtpoa])->sum('valor');

            echo "<td align=center>". number_format($Avancet2, 0, ",", ".")."</td>";

            $Avancet3=Avance::find()->where(['between','mes', "7","9"])->andFilterWhere(['id_dtpoa' => $value->iddtpoa])->sum('valor');

            echo "<td align=center>". number_format($Avancet3, 0, ",", ".") ."</td>";

            $Avancet4=Avance::find()->where(['between','mes', "10","12"])->andFilterWhere(['id_dtpoa' => $value->iddtpoa])->sum('valor');

            echo "<td align=center>". number_format($Avancet4, 0, ",", ".") ."</td>";

            $Avance=$Avancet1+$Avancet2+$Avancet3;


            $efectividad=$Avance/$meta_anual*100;

            echo "<td align=center>". number_format($efectividad, 2, ",", ".")."%</td>";


        echo "</tr>";
      }

    ?>





    </tr>




</table>

</div>
