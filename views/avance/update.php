<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Avance */

$this->title = 'Actualizar Avance: ' . $model->idavance;
$this->params['breadcrumbs'][] = ['label' => 'Avances', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idavance, 'url' => ['view', 'id' => $model->idavance]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="avance-update">

    <h3 class="modal-header-danger"><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
