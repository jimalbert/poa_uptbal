<?php

use yii\helpers\Html;
use app\models\Dtpoa;


/* @var $this yii\web\View */
/* @var $model app\models\Avance */

$this->title = 'Registrar Avance';
$this->params['breadcrumbs'][] = ['label' => 'Avances', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$iddtpoa2=Dtpoa::find()->where(['iddtpoa' => $iddtpoa])->one();
//echo $iddtpoa2->actividad;
?>
<div class="avance-create">

   <h3 class="modal-header-danger">
     ACCIÓN: <?= Html::encode(strtoupper($iddtpoa2->actividad)) ?> </br>
     UNIDAD DE MEDIDA: <?= Html::encode(strtoupper($iddtpoa2->idUnidadMedida->descripcion)) ?>                                
   </h3>
    <?= $this->render('_form2', [
        'model' => $model,
        'actividad'=>$iddtpoa2->actividad,
        'actividadid'=>$iddtpoa2->iddtpoa,
    ]) ?>

</div>
