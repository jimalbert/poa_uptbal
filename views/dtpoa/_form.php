<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

use app\models\Poa;
use app\models\Unidadmedida;
/* @var $this yii\web\View */
/* @var $model app\models\Dtpoa */
/* @var $form yii\widgets\ActiveForm */

$jsc = <<< JS


  function calculate(valor){


    enero=$('#dtpoa-enero').val();

    febrero=$('#dtpoa-febrero').val();
    marzo=$('#dtpoa-marzo').val();
    abril=$('#dtpoa-abril').val();
    mayo=$('#dtpoa-mayo').val();
    junio=$('#dtpoa-junio').val();
    julio=$('#dtpoa-julio').val();
    agosto=$('#dtpoa-agosto').val();
    septiembre=$('#dtpoa-setiembre').val();
    octubre=$('#dtpoa-octubre').val();
    noviembre=$('#dtpoa-noviembre').val();
    diciembre=$('#dtpoa-diciembre').val();


    enero = enero == "" ? 0 : Number(enero.split(",").join(""));
    febrero = febrero == "" ? 0 : Number(febrero.split(",").join(""));
    marzo = marzo == "" ? 0 : Number(marzo.split(",").join(""));
    abril = abril == "" ? 0 : Number(abril.split(",").join(""));
    mayo = mayo == "" ? 0 : Number(mayo.split(",").join(""));
    junio = junio == "" ? 0 : Number(junio.split(",").join(""));
    julio = julio == "" ? 0 : Number(julio.split(",").join(""));
    agosto = agosto == "" ? 0 : Number(agosto.split(",").join(""));
    septiembre = septiembre == "" ? 0 : Number(septiembre.split(",").join(""));
    octubre = octubre == "" ? 0 : Number(octubre.split(",").join(""));
    noviembre = noviembre == "" ? 0 : Number(noviembre.split(",").join(""));
    diciembre = diciembre == "" ? 0 : Number(diciembre.split(",").join(""));


    total= enero + febrero +  marzo + abril + mayo + junio + julio + agosto + septiembre + octubre + noviembre + diciembre;
    $('#dtpoa-meta_anual').val(total);
  }



JS;

$this->registerJs($jsc, $this::POS_END);


if (Yii::$app->user->identity->rol=="SUPERVISOR") {
  $data=ArrayHelper::map(Poa::find()->joinWith('idUnidad')
  ->where(['id_usuario' => Yii::$app->user->identity->id])->orderBy('id_unidad ASC')->all(), 'idpoa','ano');
}else {
  $data=ArrayHelper::map(Poa::find()->orderBy('id_unidad ASC')->all(), 'idpoa','ano','idUnidadDesc');
}

?>

<div class="dtpoa-form">

    <?php $form = ActiveForm::begin(); ?>


    <?=  $form->field($model, 'id_poa')->widget(Select2::classname(), [
        'data' => $data,
        'language' => 'es',
        'options' => ['placeholder' => 'Selecione una opción ...',

        ],
        'pluginOptions' => [
            'allowClear' => true,
        ],
    ]);
    ?>

    <?=  $form->field($model, 'informe_gestion')->widget(Select2::classname(), [
        'data' => ['SI'=>'SI','NO'=>'NO'],
        'language' => 'es',
        'options' => ['placeholder' => 'Selecione una opción ...',

        ],
        'pluginOptions' => [
            'allowClear' => true,
        ],
    ]);
    ?>

    <?= $form->field($model, 'actividad')->textarea(['rows' => 6]) ?>


    <?=  $form->field($model, 'id_unidad_medida')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Unidadmedida::find()->orderBy('descripcion ASC')->all(), 'idunidadmedida','descripcion'),
        'language' => 'es',
        'options' => ['placeholder' => 'Selecione una opción ...',

        ],
        'pluginOptions' => [
            'allowClear' => true,
        ],
    ]);
    ?>


    <table class="table table-striped">
        <tr>
            <td colspan="12" class="danger text-danger">
                <strong>    RESUMEN DE LA PLANIFICACION </strong>
            </td>
        </tr>

        <tr class="warning">
            <td >
                     <?= $form->field($model, 'enero')->textInput(['onchange' => 'calculate($(this))',]) ?>
            </td>

            <td >
                      <?= $form->field($model, 'febrero')->textInput(['onchange' => 'calculate($(this))',]) ?>
            </td>

            <td >
                     <?= $form->field($model, 'marzo')->textInput(['onchange' => 'calculate($(this))',]) ?>
            </td>

            <td >
                     <?= $form->field($model, 'abril')->textInput(['onchange' => 'calculate($(this))',]) ?>
            </td>

            <td >
                     <?= $form->field($model, 'mayo')->textInput(['onchange' => 'calculate($(this))',]) ?>
            </td>

            <td >
                     <?= $form->field($model, 'junio')->textInput(['onchange' => 'calculate($(this))',]) ?>
            </td>

            <td >
                      <?= $form->field($model, 'julio')->textInput(['onchange' => 'calculate($(this))',]) ?>
            </td>

            <td >
                     <?= $form->field($model, 'agosto')->textInput(['onchange' => 'calculate($(this))',]) ?>
            </td>

            <td >
                     <?= $form->field($model, 'setiembre')->textInput(['onchange' => 'calculate($(this))',]) ?>
            </td>

            <td >
                     <?= $form->field($model, 'octubre')->textInput(['onchange' => 'calculate($(this))',]) ?>
            </td>

            <td >
                     <?= $form->field($model, 'noviembre')->textInput(['onchange' => 'calculate($(this))',]) ?>
            </td>
            <td >
                     <?= $form->field($model, 'diciembre')->textInput(['onchange' => 'calculate($(this))',]) ?>
            </td>
        </tr>

    </table>




    <?= $form->field($model, 'meta_anual')->textInput(['readonly' => true,]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Registrar' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
