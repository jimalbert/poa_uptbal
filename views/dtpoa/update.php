<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Dtpoa */

$this->title = 'Actualizar Actividad: ' . $model->iddtpoa;
$this->params['breadcrumbs'][] = ['label' => 'Actividades', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->iddtpoa, 'url' => ['view', 'id' => $model->iddtpoa]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="dtpoa-update">

   <h3 class="modal-header-danger"><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
