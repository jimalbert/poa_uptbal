<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Dtpoa */

$this->title = 'Registrar Acción';
$this->params['breadcrumbs'][] = ['label' => 'Dtpoas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dtpoa-create">

    <h3 class="modal-header-danger"><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
