<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Dtpoa */

$this->title = 'Identificador del POA: '.$model->iddtpoa;
$this->params['breadcrumbs'][] = ['label' => 'Actividades', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dtpoa-view">

   <h3 class="modal-header-danger"><?= Html::encode($this->title) ?></h3>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->iddtpoa], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->iddtpoa], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'iddtpoa',

            [
                'attribute' => 'idPoa.idUnidadDesc',
                'label'     => 'POA',

                ],

            'actividad:ntext',
            //'id_unidad_medida',
            [
                'attribute' => 'idUnidadMedida.descripcion',
                'label'     => 'Unidad de Medida',

                ],
            'enero',
            'febrero',
            'marzo',
            'abril',
            'mayo',
            'junio',
            'julio',
            'agosto',
            'setiembre',
            'octubre',
            'noviembre',
            'diciembre',
            'meta_anual',
        ],
    ]) ?>

</div>
