<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "unidadresponsable".
 *
 * @property integer $idunidadresponsble
 * @property string $descripcion
 * @property string $definicion
  * @property string $vision
  * @property string $mision
  * @property string $objetivo_general
  * @property string $objetivos_especificos
  * @property string $funciones
  * @property string $lineas_estrategicas
 *
 * @property Poa[] $poas
 */
class Unidadresponsable extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'unidadresponsable';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
          [['descripcion', 'definicion', 'vision', 'mision', 'objetivo_general', 'objetivos_especificos', 'funciones', 'lineas_estrategicas'], 'required'],
         [['definicion', 'vision', 'mision', 'objetivo_general', 'objetivos_especificos', 'funciones', 'lineas_estrategicas'], 'string'],
            [['descripcion'], 'string', 'max' => 200],
            [['descripcion'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idunidadresponsble' => 'Id',
            'descripcion' => 'Unidad Responsable',
            'definicion' => 'Definición de la dependencia',
           'vision' => 'Visión Compartida',
           'mision' => 'Misión',
           'objetivo_general' => 'Objetivo General de la dependencia',
           'objetivos_especificos' => 'Objetivos Específicos de la dependencia',
           'funciones' => 'Funciones de la dependencia',
           'lineas_estrategicas' => 'Líneas Estratégicas de la dependencia',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPoas()
    {
        return $this->hasMany(Poa::className(), ['id_unidad' => 'idunidadresponsble']);
    }

    public function getAreaaccionUnidadesponsables()
  {
      return $this->hasMany(Areaaccionunidadesponsable::className(), ['id_unidadresponsble' => 'idunidadresponsble']);
  }
}
