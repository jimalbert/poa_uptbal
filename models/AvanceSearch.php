<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Avance;

/**
 * AvanceSearch represents the model behind the search form about `app\models\Avance`.
 */
class AvanceSearch extends Avance
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idavance', 'id_dtpoa', 'mes', 'valor'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Avance::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (Yii::$app->user->identity->rol=="SUPERVISOR") {
          $query->joinWith('idDtpoa.idPoa.idUnidad')
          ->where(['id_usuario' => Yii::$app->user->identity->id]);

        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idavance' => $this->idavance,
            'id_dtpoa' => $this->id_dtpoa,
            'mes' => $this->mes,
            'valor' => $this->valor,
        ]);

        return $dataProvider;
    }
}
