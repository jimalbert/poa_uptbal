<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "areaaccion_unidadesponsable".
 *
 * @property int $idareaaccionunidadesponsable
 * @property int $id_areaccion
 * @property int $id_unidadresponsble
 * @property int $id_usuario
 *
 * @property Areaccion $areaccion
 * @property Unidadresponsable $unidadresponsble
 * @property Usuario $usuario
 */
class Areaaccionunidadesponsable extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'areaaccion_unidadesponsable';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_areaccion', 'id_unidadresponsble', 'id_usuario'], 'required'],
            [['id_areaccion', 'id_unidadresponsble', 'id_usuario'], 'integer'],
            [['id_areaccion'], 'exist', 'skipOnError' => true, 'targetClass' => Areaccion::className(), 'targetAttribute' => ['id_areaccion' => 'idareaccion']],
            [['id_unidadresponsble'], 'exist', 'skipOnError' => true, 'targetClass' => Unidadresponsable::className(), 'targetAttribute' => ['id_unidadresponsble' => 'idunidadresponsble']],
            [['id_usuario'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['id_usuario' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idareaaccionunidadesponsable' => 'Idareaaccionunidadesponsable',
            'id_areaccion' => 'Area de acción',
            'id_unidadresponsble' => 'Unidad Responsable',
            'id_usuario' => 'Usuario',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAreaccion()
    {
        return $this->hasOne(Areaccion::className(), ['idareaccion' => 'id_areaccion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnidadresponsble()
    {
        return $this->hasOne(Unidadresponsable::className(), ['idunidadresponsble' => 'id_unidadresponsble']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'id_usuario']);
    }

    public function getUnidadnombre()
    {
        return $this->unidadresponsble->descripcion;
    }

    public function getAreaccionnombre()
    {
        return $this->areaccion->descripcion;
    }
}
